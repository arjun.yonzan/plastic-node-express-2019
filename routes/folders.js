var express = require('express');
var router = express.Router();
var Model = require('../models/all');


//FOLDERS
router.get('/', function(req, res, next) {

	var userId = req.query.userId;
	Model.Folder.find({
		userId: userId
	}, function(err, folders) {
		if (err) {
			res.status(400).json(err);
		}
		res.json(folders);
	});

});


router.post('/', function(req, res, next) {

	req.auth().then(function(session) {
		var input = req.body;
		var folder = new Model.Folder({
			title: input.title,
			level: input.level,
			parent: input.parent,
			userId: session.userId
		});

		folder.save(function(err,result) {
			if (err) {
				res.send("error creating folder", 400);
			}
			res.json(folder);
		});
	});

});

router.put('/:folderId', function(req, res, next) {

	req.auth().then(function(session) {
		var id = req.params.folderId;
		var values = req.body;
		values.updated_date = new Date;
		values.userId = session.userId;

		Model.Folder.update({
			_id: id
		}, values, function(err, result) {
			res.send(result);
		});
	});

});

router.delete('/:folderId', function(req, res, next) {

	req.auth().then(function() {
		var folderId = req.params.folderId;
		Model.Folder.find({
			"parent": folderId
		}, function(err, folders) {
			if (folders.length) {
				res.json({
					folderEmpty: false
				}, 400);
			} else {
				Model.Folder.remove({
					_id: folderId
				}, function(err, result) {
					if (err) {
						res.json(err, 404);
					}
					res.json(result);
				});
			}
		});
	});


});


module.exports = router;