var express = require('express');
var router = express.Router();
var Model = require('../models/all');
const fs = require('fs');
var multer = require('multer');
var upload = multer({
	dest: 'tmp_uploads/'
});
var elasticClient = require('../elasticsearch');
var Promise = require("bluebird");

/*Gmail Credentials Setup 27 December, 2021*/
/*https://developers.google.com/gmail/api/quickstart/nodejs*/
const readline = require('readline');
const {google} = require('googleapis');
let oAuth2Client
const SCOPES = ['https://www.googleapis.com/auth/gmail.send'];
const TOKEN_PATH = 'token.json';


// Load client secrets from a local file.
fs.readFile('credentials.json', (err, content) => {
  if (err) return console.log('Error loading client secret file:', err);
  authorize(JSON.parse(content));
});

/*authorize*/
function authorize(credentials) {
  const {client_secret, client_id, redirect_uris} = credentials.web;
  oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client);
    oAuth2Client.setCredentials(JSON.parse(token));
  });
}

function getNewToken(oAuth2Client) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
    });
  });
}


/*email...*/
/*---------------------------------------------------------------*/

async function sendEmail(text){
	const gmail = google.gmail({version: 'v1', auth:oAuth2Client})
	let encoded = Buffer.from(text).toString('base64').replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '');

	try{
		let email = await gmail.users.messages.send({
			userId: "me",
			resource:{
				raw: encoded
			}

		})
		return email
	}catch(err){
		throw err;
	}
} //end sendEmail


/*router.get('/mail', async (req,res)=>{
 	let text = `From: arjunyonjan.tech@gmail.com
To: arjun.yonzan@gmail.com
Subject: Welcome to Contain App
Content-Type: text/html; charset=UTF-8

<img src="https://res.cloudinary.com/dpnxmo8ak/image/upload/c_scale,h_60/v1640594297/ContainApp/contain-logo_v8o5h2.jpg">
<br>
<p><strong>Thanks</strong> for registering on codecontain.xyz</p>

<br>

<p>CodeContain Team</p>`

	try{
		let email = await sendEmail(text)
		res.json({email})
	}catch(err){
		res.status(400).json({error:err.message})
	}	

})*/
/*--------------------------------------------------------------*/

function randString(x) {
	var s = "";
	while (s.length < x && x > 0) {
		var r = Math.random();
		s += (r < 0.1 ? Math.floor(r * 100) : String.fromCharCode(Math.floor(r * 26) + (r > 0.5 ? 97 : 65)));
	}
	return s;
}

router.get('/', function(req, res, next) {
	res.send({
		contain: true
	});
});

//REGISTRATION...
router.post('/register', async function(req, res, next) {
	let registerData = req.body;
	registerData.isGoogleLogin=false
	try{
		await handleRegistration(registerData)
		res.json({success:true})
	}catch(err){
		res.status(400).json({error:err})
	}

});


/*--------------------------------------------------------------*/
function handleRegistration(userData){
	return new Promise((resolve, reject) => {

		const {username, email} = userData
		console.log({username,email}, "$$$$")

		Model.User.findOne({
			$or: [{
		        username: username
		    }, {
		        email: email
		    }]
		}, async function(err, user) {
			console.log(user, "----CHECK --------------------------------------------------------------$$$$")

			if (!user) {
				try{
					const response = await createNewAccount(userData)
					let text = `From: arjunyonjan.tech@gmail.com
To: ${response.email}
Subject: Welcome to Contain App
Content-Type: text/html; charset=UTF-8

<div style="font-size: 16px;">
<img src="https://res.cloudinary.com/dpnxmo8ak/image/upload/c_scale,h_60/v1640594297/ContainApp/contain-logo_v8o5h2.jpg">
<br>
<p><strong>Thanks</strong> for registering on "Contain" app.</p>

<br>

<p>Your username is <strong>${response.username}</strong></p>
<p>Your password is <strong>${response.password}</strong></p>
<p>Your email is <strong>${response.email}</strong></p>
<p>Access your account at https://contain.live/${user.username}</p>

<br>

<p>Contain Team</p>
</div>`;
					
					try{
						await sendEmail(text)
					}catch(err){
						throw err
					}

					resolve(response)
				}catch(err){
					reject(err)
				}
			} else {
				reject("username or email already exists")
				// throw "user already exits"
			}
		});

	});
}

function createNewAccount({email, username, password}){

	return new Promise((resolve, reject) => {
		new Model.User({email, username, password}).save(function(err, user) {
			if (err) {throw err};
			resolve(user)
			/*ISSUE 405 -- may be architecture is changed 26 December, 2021*/
			/*elasticClient.indices.create({
				index: 'plastic_' + "techyonjan"
			}, function(err, result) {
				if (err) {throw err}
				resolve(result)
			});*/
		});
	})
}
/*--------------------------------------------------------------*/


/*GOOGLE LOGIN - NEW REGISTRATION - TRY 26 December, 2021*/
router.post('/login/google',(req,res)=>{
	console.log(req.body, "$$$$")
})


//LOGIN
const {OAuth2Client} = require('google-auth-library');

router.post('/login', function(req, res, next) {

	/*google auth handle 24 December, 2021*/
	const CLIENT_ID = "179265441527-8udm2anht01i9cuo1cjq4h6c6e71jfh9.apps.googleusercontent.com"
	const client = new OAuth2Client(CLIENT_ID);
	
	let {auth, idToken, email_user, password} = req.body

	/*--------------------------------------------------------------*/
	if (auth === "GOOGLE") {
	    async function verify() {
	        try {
	            const ticket = await client.verifyIdToken({
	                idToken: idToken,
	                audience: CLIENT_ID
	            });
	            const payload = ticket.getPayload();
	            const email = payload['email'];
	            const verified = payload['email_verified']
	            const googleName= payload['name']

	            if(verified){
	            	dothis({email_user: email, password: null, isGoogleLogin:true, googleName:googleName})

	            }else{
	            	throw "GOOGLE ACCOUNT NOT VERIFIED"
	            }


	        } catch (err) {
	            res.status(401).json({ err: err.message })
	        }
	    }
    	verify()
	}else{
		dothis({email_user,password})
	}

	/*--------------------------------------------------------------*/
	async function dothis({email_user, password, isGoogleLogin, googleName}){

		Model.User.findOne({
		    $or: [{
		        username: email_user
		    }, {
		        email: email_user
		    }]
		}, async function(err, user) {

		    if (err) {
		        res.status(400).json({
		            error: 'login error'
		        });
		    };

		    /* FOR GOOGLE AUTHENTICATION */
		    if(isGoogleLogin && !user){
		    	/*if the user is null -- that is new user...*/
				console.log("GOOGLE REGISTER NEW PLEASE ------------------------------------", "$$$$") 	
				let username = googleName.replace(/\s/g, '').trim().toLowerCase()

				function getRandomInt(max) {
				  return Math.floor(Math.random() * max);
				}

				let newUser = {
					email: email_user,
					password: randString(10),
					username: `${username}${getRandomInt(1000)}`
				}

				try{
					const user = await handleRegistration(newUser)
					/*if response success -- create sesion and login*/
					handleSession(user)

				}catch(err){
					res.status(400).send(err)
				}	   

				return
		    }


		    /*Already USER && isGoogleLogin*/
		    if(isGoogleLogin){
		    	handleSession(user)
		    	return
		    }

		    /*--------------------------------------------------------------*/

		    if (!user) {
		        res.status(400).send({
		            noUser: true
		        });
		    }


		    if (user && password) {
		        if (password === user.password) {
		            handleSession(user)
		        } else {
		            res.status(400).json({
		                msg: "wrong username password"
		            });
		        }
		    }
		});
	}

	function handleSession(user){
		console.log("handing --------------------------------------------------------------", "$$$$")
		var token = randString(100);
        var auth = {
            userId: user._id,
            username: user.username,
            token: token,
            // expires: (new Date).getTime() * 1 * 60 * 60 * 1000
        };

		Model.Session.findOne({
            username: user.username
        }, function(err, session) {
            if (err) {
                console.log("here.200...", "$$$$");
                res.status(400).send(err);
            }

            if (session) {
                res.json(session);
            } else {
                createSession();
            }
        });

        function createSession() {
            console.log(auth, "creating auth..---$$$$");
            var session = new Model.Session(auth);
            session.save(function(err) {
                if (err) {
                    console.log("here....", "$$$$");
                    res.status(400).send(err);
                }
                res.json(auth);
            });
        }
	}




});

router.get('/logout', function(req, res, next) {
	req.auth().then(function(session) {
		Model.Session.remove({
			token: session.token
		}, function(err, result) {
			if (err) {
				res.status(400).json(err);
			}
			res.json({
				success: true,
				msg: "sesion revoked"
			});
		});
	});
});

router.get('/user', function(req, res, next) {
	var username = req.query.username;
	Model.User.findOne({
		username: username
	}, function(err, user) {
		if (err) {
			res.json(err);
		}
		if (user) {
			res.json({
				username: user.username,
				userId: user._id
			});
		} else {
			res.status(400).json({
				noUser: true,
				msg: "user does not exists"
			})
		}
	});
});

/*router.get('/elastic', function(req, res, next) {
	Model.User.find({}, function(err, users) {
		if (err) {
			throw err
		}
		users.forEach(function(user, userIndex) {
			var index = "plastic_" + user.username;
			elasticClient.indices.exists({
				index: index
			}, function(err, found) {

				if (found) {
					elasticClient.indices.delete({
						index: index
					}, function(err, result) {
						if (result.acknowledged) {
							console.log("index deleted...");
							createIndex();
						}
					});
				} else {
					createIndex();
				}

			})

			function createIndex() {
				console.log("index creating...");
				elasticClient.indices.create({
					index: index,
					body: {
						settings: {
							number_of_shards: 3,
							analysis: {
								filter: {
									ngram_filter: {
										type: "ngram",
										min_gram: 2,
										max_gram: 20
									}
								},
								analyzer: {
									ngram_analyzer: {
										type: "custom",
										tokenizer: "standard",
										filter: [
											"lowercase",
											"ngram_filter"
										]
									}
								}
							}
						},
						mappings: {
							codes: {
								_all: {
									type: "string",
									analyzer: "ngram_analyzer"
								}
							}
						}
					}
				}, function(err, result) {

					if (result.acknowledged) {
						console.log("index created...");
						indexCodes(user).then(function() {
							if (userIndex == users.length - 1) {
								res.json({
									success: true
								});
							}
						});


					}
				});
			}
		});
	});


	function indexCodes(user) {
		return new Promise(function(resolve, reject) {
			var index = "plastic_" + user.username;

			Model.Code.find({
				'user.username': user.username
			}, function(err, codes) {

				if (err) {
					throw err;
				}

				codes.map(function(code, codeIndex) {

					var elasticCode = {};
					elasticCode.id = code._id;
					elasticCode.title = code.title;
					elasticCode.tags = code.tags;
					elasticCode.user = code.user;

					code.contents.map(function(content, index) {
						elasticCode['content' + index] = content;
					});

					elasticClient.index({
						index: index,
						type: "codes",
						id: String(elasticCode.id),
						body: elasticCode
					}).then(function(result) {

						if (codeIndex == codes.length - 1) {
							console.log("completed indexing for " + user.username);
							resolve();
						}

					});

				});

			});
		});
	}
});*/

router.get('/elastic/index',(req,res)=>{
			/*updated 28 December, 2021*/
				console.log("index creating...");
				elasticClient.indices.create({
					index: "contain",
					body: {
						 "settings": {
					    "analysis": {
					      "analyzer": {
					        "my_analyzer": {
					          "tokenizer": "my_tokenizer"
					        }
					      },
					      "tokenizer": {
					        "my_tokenizer": {
					          "type": "ngram",
					          "min_gram": 2,
					          "max_gram": 3,
					          "token_chars": [
					            "letter",
					            "digit"
					          ]
					        }
					      }
					    }
					  },
					   "mappings": {
					    "properties": {
					      "title": {
					        "type": "text",
					        "analyzer": "my_analyzer"
					      },
					      "tags": {
					        "type": "text",
					        "analyzer": "my_analyzer"
					      },
					      "content": {
					        "type": "text",
					        "analyzer": "my_analyzer"
					      },
					      "user.username": {
					        "type": "text",
					        "analyzer": "whitespace"
					      }
					    }
					  }

					}
				}, function(err, result) {

					if(err){
						console.log(55000, "$$$$")
						// throw err
						res.send(err)
					}


					console.log(result, "the result......--------------------------------------------------------------$$$$")

					if (result.body.acknowledged) {

						console.log(65000, "$$$$")
						console.log("index created...");
						res.json({
									success: true
								});


					}
				});


})


router.get('/elastic/delete',(req,res,next)=>{
	console.log("deleting.....", "$$$$")
	elasticClient.indices.exists({
		index: "contain"
	}, function(err, found) {

		if(err){
			res.send(err)
		}

		if (found) {
			console.log("found....", "$$$$")
			elasticClient.indices.delete({
				index: "contain"
			}, function(err, result) {

				if(err){
					res.send(err)
				}

				console.log(result, "$$$$")

				if (result.body.acknowledged) {


					/*--------------------------------------------------------------*/

					elasticClient.indices.create({
					index: "contain",
					body: {
						 "settings": {
					    "analysis": {
					      "analyzer": {
					        "my_analyzer": {
					          "tokenizer": "my_tokenizer"
					        }
					      },
					      "tokenizer": {
					        "my_tokenizer": {
					          "type": "ngram",
					          "min_gram": 2,
					          "max_gram": 3,
					          "token_chars": [
					            "letter",
					            "digit"
					          ]
					        }
					      }
					    }
					  },
					   "mappings": {
					    "properties": {
					      "title": {
					        "type": "text",
					        "analyzer": "my_analyzer"
					      },
					      "tags": {
					        "type": "text",
					        "analyzer": "my_analyzer"
					      },
					      "content": {
					        "type": "text",
					        "analyzer": "my_analyzer"
					      },
					      "user.username": {
					        "type": "text",
					        "analyzer": "whitespace"
					      },
					      "user.userId": {
					        "type": "text",
					        "analyzer": "whitespace"
					      }
					    }
					  }

					}
				}, function(err, result) {

					if(err){
						console.log(55000, "$$$$")
						// throw err
						res.send(err)
					}

					console.log(result, "the result......--------------------------------------------------------------$$$$")
					if (result.body.acknowledged) {

							console.log(65000, "$$$$")
							console.log("index created...");
							res.json({
								success: true,
								message: "deleted and created new index 'contain'"
							});


						}
					});
					/*--------------------------------------------------------------*/


				}
			});
		} else {
			res.status(400).send("No indexes")
		}			

	})
})


router.get('/elastic/all', function(req, res, next) {

	let indexedCounter = 0

	Model.User.find({}, function(err, users) {
		if (err) {
			throw err
		}
		users.forEach(function(user, userIndex) {
			var index = "contain";

			indexCodes(user,userIndex).then(function() {

				indexedCounter++

				console.log(parseFloat(indexedCounter/users.length*100).toFixed(2), "% Indexed --------------------------------------------------------------$$$$")

				if (indexedCounter === users.length) {
					console.log("Elastic Indexes Completes....", "$$$$")
					res.json({
						success: true,
						message: "User Codes Indexed on elastic search"
					});
				}
			},()=>{
				throw "INDEXING ERROR"
				console.log("somethings wrong...", "INDEXING ERROR" , "$$$$")
			});

		});//end users foreach..
	});


	function indexCodes(user, userIndex) {
		return new Promise(function(resolve, reject) {

			Model.Code.find({
				'user.userId': user._id.toString()
			}, function(err, codes) {

				if (err) {
					throw err;
				}

				/*missed it before -- new fix 28 December, 2021*/
				if(!codes.length){
					// console.log("completed indexing for " + user.username);
					resolve()
				}

				codes.map(function(code, codeIndex) {

					var elasticCode = {};
					elasticCode.id = code._id;
					elasticCode.title = code.title; //title
					elasticCode.tags = code.tags; //tags
					elasticCode.user = code.user; //user info...

					let content="";
					code.contents.map(function(codeContent, index) {
						let label = codeContent.label?codeContent.label:""
						let body = codeContent.body?codeContent.body:""
						let lang = codeContent.lang?codeContent.lang:""
						content += " " + label + " " + body + " " + lang
					});
					elasticCode.content = content

					//elastic indexing...
					elasticClient.index({
						index: 'contain',
						type: "_doc",
						id: String(elasticCode.id),
						body: elasticCode
					}).then(function(result,err) {

						if(err){throw err}

						if (codeIndex == codes.length - 1) {
							// console.log("completed indexing for " + user.username);
							resolve();
						}
					});
				});
			});
		});
	}
});



router.post('/txtfile', upload.any(), function(req, res, next) {
	var files = req.files;
	files.forEach(function(file) {
		var uploadedFile = "tmp_uploads/" + file.filename;

		fs.readFile(uploadedFile, "utf8", (err, data) => {
			if (err) throw err;
			fs.unlink(uploadedFile);
			res.json({
				content: data
			});
		});
	});
});

//search api
router.post('/search', async function(req, res, next) {
	let { body } = req
	if (!body.userId) {
		res.status(404).json({ error: "no user given" })
	}
	try {

		//query for elastic search
		//and search
		//multimatch
		//username and multimatch query // bool query...

		console.log(body.userId, "------user id------$$$$")

		const query = {
			"from" : 0, 
			"size" : 50,
			"query": {
				"bool": {
					"must": [
						{
							"multi_match": {
								"query": body.q,
								"fields": ["title", "content", "tags"]
							}
						},
						{
							"term": {
								"user.userId": body.userId
							}
						}
					]
				}
			}//end query
		}

		//elasitic client search api...
		const response = await elasticClient.search({
			index: 'contain',
			type: '_doc',
			body: query
		})//end search
		res.json(response)
	} catch (err) {
		res.status(400).json({ message: "elastic search error", error: err })
	}

});


router.get('/users',function (req, res, next){

	Model.Profile.find({}, function (err, profiles){
	
		if(err){ res.status(400).json(err)}
		res.json(profiles);
			
	});

});

router.get('/profile/:userId', function(req, res, next){
	Model.Profile.findOne({userId: req.params.userId }, function (err, profile){
		res.json(profile);	
	});
});

router.put('/profile/:userId', function(req, res, next){
	var data = {
		name: req.body.name,
		email: req.body.email,
		address: req.body.address,
		skills: req.body.skills,
		profession: req.body.profession,
		linkedin: req.body.linkedin,
		facebook: req.body.facebook
	};

	req.auth().then(function(session){
		if(session.userId === req.params.userId){
			Model.Profile.update({userId: req.params.userId},data , function(err, profile){
				res.json({success:true});
			});
		}else{
			res.status(400).json({msg:"Profile property violation"});
		}
		
	});
		
});



/*--------------------------------------------------------------*/


router.post('/reset/password', async (req,res)=>{
	let {email} = req.body
	try{
	 	const user = await Model.User.findOne({
			email: email
		})

	 	if(!user){
				res.status(404).json({error: "email not found"})
				return
	 	}

		let text = `From: arjunyonjan.tech@gmail.com
To: ${user.email}
Subject: Reset Password Information - Contain App
Content-Type: text/html; charset=UTF-8

<div style="font-size: 16px;">
	<img src="https://res.cloudinary.com/dpnxmo8ak/image/upload/c_scale,h_60/v1640594297/ContainApp/contain-logo_v8o5h2.jpg">
	<br>
	<p>Here are your account details for "Contain" app.</p>

	<br>

	<p>Your username is <strong>${user.username}</strong></p>
	<p>Your password is <strong>${user.password}</strong></p>
	<p>Your email is <strong>${user.email}</strong></p>

	<p>Access your account at https://contain.live/${user.username}</p>

	<br>

	<p>Thank you! <br/> Contain Team</p>
</div>`;		
			try{
				await sendEmail(text)
				res.json({success:true})
			}catch(err){
				res.status(404).json({error: "Error Reseting with Email. Please Try Again."})
			}

	}catch(err){
		res.status(404).json({error: "email not found"})
	}
})//reset password


/*--------------------------------------------------------------*/
router.get('/test', function(req, res, next) {

	Model.Code.find({'user.userId': "577936740a62442d570f63a3"}, function (err, result){
		if(err){throw err}
		console.log(result);
		res.json({result})	
	}).limit(1);

});

/*--------------------------------------------------------------*/

router.put("/account/:userId/username", async (req,res,next)=>{

	try{

		const session = await req.auth()
		const {userId} = session

		const newUsername = req.body.username

			Model.User.findOne({username: newUsername}, function (err, result){

			if(err){throw err; res.error(400).json(err)}

			if(result){
    		res.status(400).json({
    			error: "USERNAME_EXISTS"
    		});
			}else{

				/*new user name to be updated......*/
				Model.User.update({_id:userId}, {username:newUsername} ,function (err, result){
					if(err){throw err; res.error(400).json(err)}
					res.json({result})
				});
			}		
		});



	}catch(err){
		
		throw err
		res.json(err)
	}
})



module.exports = router;