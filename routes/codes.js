var express = require('express');
var router = express.Router();
var Model = require('../models/all');
var elasticClient = require('../elasticsearch');

router.get('/', function(req, res, next) {

	var userId = req.query.userId;
	var folderId = req.query.folderId;
	var options = {};
	var page = parseInt(req.query.page);
	var limit = parseInt(req.query.limit);
	var skip = 0;

	if (page > 1) {
		skip = (page - 1) * limit;
	}
	if (!limit) {
		limit = 14;
	}

	if (userId) {
		options['user.userId'] = userId;
	}

	if (folderId) {
		options.folder = folderId;
	}

	req.auth().then(function(session) {
		if (userId !== session.userId) {
			options.private = false;
		}
		getCodes(options);
	}, function() {
		options.private = false;
		getCodes(options);
	});

	function getCodes(options) {
		Model.Code.find(options).sort({
			'updated_date': -1
		}).skip(skip).limit(limit).exec(function(err, codes) {
			if (err) {
				res.status(400).json(err);
			}
			Model.Code.count(options, function(err, count) {
				res.json({
					codes: codes,
					total: count
				});
			});
		});
	}

});

router.get('/all', function(req, res, next) {
	Model.Code.find({
		private: false
	}).sort({
		'updated_date': -1
	}).limit(10).exec(function(err, codes) {
		if (err) {
			res.status(400).json(err);
		}
		res.json(codes);
	});
});


router.get('/:code_id', function(req, res, next) {
	var codeId = req.params.code_id;
	var options = {
		_id: codeId
	};
	req.auth().then(function(session) {
		getCode(options);
	}, function() {
		options.private = false;
		console.log(options, "2222");
		getCode(options);
	});

	function getCode(options) {
		Model.Code.findOne(options).populate('user1', 'username _id').exec((err, code)=>{
			if (err) {
				res.status(400).json(err);
			}
			if (code) {
				res.json(code);
			} else {

				if (!options.private) {
					res.status(400).json({
						msg: "private code is not available."
					});
				} else {
					res.status(400).json({
						msg: "code not found"
					});
				}

			}
		});
	}

});

router.post('/', function(req, res, next) {
	req.auth().then(function(session) {

		var postData = req.body;
		var code = new Model.Code({
			title: postData.title,
			contents: postData.contents,
			folder: postData.folder,
			tags: postData.tags,
			private: postData.private,
			user: {
				username: session.username,
				userId: session.userId
			},
			user1:session.userId,
			updated_date: new Date
		});

		code.save(function(err, result) {

			if (err) {
				res.send("error");
			}

			var elasticCode = {};
			elasticCode.id = result._id;
			elasticCode.title = result.title;
			elasticCode.tags = result.tags;
			elasticCode.user = result.user;
			let content="";
			code.contents.map(function(codeContent, index) {
				let label = codeContent.label?codeContent.label:""
				let body = codeContent.body?codeContent.body:""
				let lang = codeContent.lang?codeContent.lang:""
				content += " " + label + " " + body + " " + lang
			});
			elasticCode.content = content

			elasticClient.index({
				index: 'contain',
				type: "_doc",
				id: String(elasticCode.id),
				body: elasticCode
			}, function(err, result) {
				res.json({
					success: true,
					code: code
				});
			});

		});
	});

});


router.put('/:code_id', function(req, res, next) {
	var codeId = req.params.code_id;
	updatedCode = req.body;
	updatedCode.updated_date = new Date;

	req.auth().then(function(session) {
		Model.Code.findOne({
			_id: codeId
		}, function(err, code) {
			if (err) {
				res.status(400).json(err);
			}
			if (code) {
				if (session.userId == code.user.userId) {
					updateCode(updatedCode, session);
				} else {
					res.status(401).json({
						success: false,
						error: "UNAUTHORIZED"
					});
				}
			}
		});
	});

	function updateCode(code, session) {
		Model.Code.update({
			_id: codeId
		}, updatedCode, function(err, coderesult) {

			if (err) {
				res.status(400).json(err);
			}

			var elasticCode = {};
			elasticCode.id = updatedCode._id;
			elasticCode.title = updatedCode.title;
			elasticCode.tags = updatedCode.tags;
			elasticCode.user = updatedCode.user;
			let content="";
			code.contents.map(function(codeContent, index) {
				let label = codeContent.label?codeContent.label:""
				let body = codeContent.body?codeContent.body:""
				let lang = codeContent.lang?codeContent.lang:""
				content += " " + label + " " + body + " " + lang
			});
			elasticCode.content = content

			elasticClient.index({
				index: 'contain',
				type: "_doc",
				id: String(elasticCode.id),
				body: elasticCode
			}, function(err, result) {

				res.json({
					success: true,
					result: coderesult
				});
			});

		});
	}

});

router.delete('/:code_id', function(req, res, next) {
	var codeId = req.params.code_id;
	req.auth().then(function(session) {
		Model.Code.findOne({
			_id: codeId
		}, function(err, code) {

			if (err) {
				res.status(400).json(err);
			}
			if (code) {

				if (session.userId == code.user.userId) {
					removeCode(codeId, session);
				} else {
					res.status(400).json({
						success: false,
						msg: "not your property"
					});
				}
			}
		});
	});

	function removeCode(codeId, session) {
		Model.Code.remove({
			_id: codeId
		}, function(err) {
			if (err) {
				throw err
			}

			elasticClient.delete({
				index: 'contain',
				type: '_doc',
				id: String(codeId)
			}, function(error, response) {

				if(error){
					res.status(400).json(error)
				}

				console.log("elastic deleted....");
				res.json({
					success: true
				});
			});

		});
	}
});


module.exports = router;