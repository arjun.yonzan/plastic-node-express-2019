var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Session = mongoose.model('Session', {
	token: {
		type: String
	},
	userId: {
		type: String
	},
	username: {
		type: String
	},
	expires: {type: Date}
});

var User = mongoose.model('User', {
	username: {
		type: String
	},
	email: {
		type: String
	},
	password: {
		type: String
	},
	database: {
		type: String
	}
});

var Code = mongoose.model('Code', {
	title: {
		type: String
	},
	contents: [],
	lang: {
		type: String
	},
	folder: String,
	user: Object,
	user1: {
		type: Schema.Types.ObjectId,
		ref: "User"
	},
	tags: String,
	private: Boolean,
	updated_date: {
		type: Date
	}
});

var Folder = mongoose.model('Folder', {
	title: String,
	level: Number,
	parent: String,
	userId: String
});

var Profile = mongoose.model('Profile',{
	name: String,
	profession: String,
	company: String,
	address: String,
	skills: String,
	facebook: String,
	linkedin: String,
	email: String,
	username: String,
	userId: String 
});



module.exports = {
	Session: Session,
	User:User,
	Profile: Profile,
	Code: Code,
	Folder: Folder
}