var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Promise = require("bluebird");

var mongoose = require('mongoose');
mongoose.connect('mongodb://root:Kp2322014@localhost:2727/plastic?authSource=admin');
var Model = require('./models/all');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

//CORS middleware for expres...
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, sid");
  res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  next();
});

//Authentication Middleware
app.use(function(req, res, next) {

  req.auth = function() {
    return new Promise(function(resolve, reject) {
      var token = req.headers.authorization;
      if (token) {
        Model.Session.findOne({
          token: token
        }, function(err, session) {
          if (err) {
            console.log("error occured..");
          }
          if (session) {
            resolve(session);
          } else {
            reject();
            res.status(400).json({
              auth: false,
              msg: "token error"
            });
          }
        });

      } else {
        reject();
      }
    });
  }

  req.reject = function() {
    res.status(400).json({
      auth: false,
      msg: "token error"
    });
  };

  next();
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/folders', require('./routes/folders'));
app.use('/codes', require('./routes/codes'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
